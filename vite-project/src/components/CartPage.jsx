import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import ConfirmModal from '../components/modal/ConfirmModal';
import { removeFromCart } from '../redux/slices/cartSlice';
import { openModal, closeModal } from '../redux/slices/modalSlice';

const CartPage = () => {
    const cart = useSelector((state) => state.cart);
    const isModalOpen = useSelector((state) => state.modal.isOpen);
    const dispatch = useDispatch();
    const [itemToRemove, setItemToRemove] = useState(null);

    const handleRemoveClick = (itemId) => {
        setItemToRemove(itemId);
        dispatch(openModal());
    };

    const handleConfirmRemove = () => {
        dispatch(removeFromCart(itemToRemove));
        dispatch(closeModal());
    };

    const handleCloseModal = () => {
        dispatch(closeModal());
        setItemToRemove(null);
    };

    if (!cart || cart.length === 0) {
        return (
            <div className="cart-page">
                <h1>Cart</h1>
                <p>Your cart is empty.</p>
            </div>
        );
    }

    return (
        <div className="cart-page">
            <h1>Cart</h1>
            <div className="product-list">
                {cart.map(product => (
                    <div key={product.id} className="product-item">
                        <img src={product.imagePath} alt={product.alt} />
                        <h2>{product.name}</h2>
                        <p>{product.price}</p>
                        <button onClick={() => handleRemoveClick(product.id)}>Remove</button>
                    </div>
                ))}
            </div>
            <ConfirmModal
                isOpen={isModalOpen}
                onRequestClose={handleCloseModal}
                onConfirm={handleConfirmRemove}
            />
        </div>
    );
};

CartPage.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CartPage;
