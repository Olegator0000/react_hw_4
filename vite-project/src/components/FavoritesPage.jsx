import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { removeFromFavorites } from '../redux/slices/favoritesSlice';

const FavoritesPage = ({ favorites }) => {
    const dispatch = useDispatch();

    if (!favorites || favorites.length === 0) {
        return (
            <div className="favorites-page">
                <h1>Favorites</h1>
                <p>Your favorites list is empty.</p>
            </div>
        );
    }

    return (
        <div className="favorites-page">
            <h1>Favorites</h1>
            <div className="product-list">
                {favorites.map(product => (
                    <div key={product.id} className="product-item">
                        <img src={product.imagePath} alt={product.alt} />
                        <h2>{product.name}</h2>
                        <p>{product.price}</p>
                        <button onClick={() => dispatch(removeFromFavorites(product.id))}>Remove</button>
                    </div>
                ))}
            </div>
        </div>
    );
};

FavoritesPage.propTypes = {
    favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default FavoritesPage;
