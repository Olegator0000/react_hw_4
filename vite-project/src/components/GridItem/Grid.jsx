import React from 'react';
import GridItem from './GridItem';

function Grid({ images }) {
    return (
        <div className="grid-container">
            {images.map((image) => (
                <GridItem key={image.id} src={image.src} alt={image.alt} />
            ))}
        </div>
    );
}

export default Grid;