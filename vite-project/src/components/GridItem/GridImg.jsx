import React from 'react';
import BuyButton from '../button/BuyButton';
import FavoriteButton from '../button/FavoriteButton';
import PropTypes from 'prop-types';
import './Grid.scss';

const GridImg = ({ items, handleFavoriteButtonClick, handleBuyButtonClick, favorites }) => {
    return (
        <div className="grid">
            {items.map((image) => (
                <div key={image.id} className="grid-item"> {/* Додано key={image.id} */}
                    <img src={image.imagePath} alt={image.alt} />
                    <div className="details">
                        <h2>name: {image.name}</h2>
                        <p>Price: ${image.price}</p>
                        <p>SKU: {image.sku}</p>
                        <p>Color: {image.color}</p>
                        <BuyButton onClick={() => handleBuyButtonClick(image)} />
                        <FavoriteButton
                            onClick={() => handleFavoriteButtonClick(image)}
                            isFavorite={favorites.some(fav => fav.id === image.id)}
                        />
                    </div>
                </div>
            ))}
        </div>
    );
};

GridImg.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleFavoriteButtonClick: PropTypes.func.isRequired,
    handleBuyButtonClick: PropTypes.func.isRequired,
    favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default GridImg;
