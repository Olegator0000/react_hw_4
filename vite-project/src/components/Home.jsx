import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addToCart } from '../redux/slices/cartSlice';
import { addToFavorites } from '../redux/slices/favoritesSlice';
import GridImg from '../components/GridItem/GridImg';
const Home = () => {
    const [products, setProducts] = useState([]);
    const dispatch = useDispatch();

    const fetchProducts = async () => {
        try {
            const response = await fetch('/mock.json');
            if (!response.ok) {
                throw new Error('Failed to fetch products');
            }
            const data = await response.json();
            setProducts(data);
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    };

    useEffect(() => {
        fetchProducts();
    }, []);

    return (
        <div className="home">
            <h1>Home Page</h1>
            <GridImg
                items={products}
                handleBuyButtonClick={(item) => dispatch(addToCart(item))}
                handleFavoriteButtonClick={(item) => dispatch(addToFavorites(item))}
                favorites={[]}
            />
        </div>
    );
};

export default Home;
