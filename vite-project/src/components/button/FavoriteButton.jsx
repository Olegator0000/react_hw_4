import React from 'react';
import PropTypes from 'prop-types';

const FavoriteButton = ({ onClick, isFavorite }) => {
    return (
        <button onClick={onClick}>
            {isFavorite ? <span>❤️</span> : <span>🤍</span>}
        </button>
    );
};

FavoriteButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool.isRequired,
};

export default FavoriteButton;

