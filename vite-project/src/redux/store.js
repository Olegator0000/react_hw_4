import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './slices/cartSlice';
import favoritesReducer from './slices/favoritesSlice';
import modalReducer from './slices/modalSlice';

const store = configureStore({
    reducer: {
        cart: cartReducer,
        favorites: favoritesReducer,
        modal: modalReducer,
    },
});

export default store;
